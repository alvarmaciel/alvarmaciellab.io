+++
title = "Acerca de mi"
description = "Un poco más de información"
date = "2021-10-30"
aliases = ["about-us", "about-me", "contact"]
author = "Alvar Maciel"
+++
Soy padre de dos niñes. Programador, docente. Siempre estuve vinculado a proyectos tecnológicos. Durante el confinamiento encontré el tiempo de programar a diario y colaborar con algunas Pymes locales en sus procesos de adaptación tecnológica. Programo principalmente en Python. Uso Jupyter para enseñar y analizar datos. Django para el backend de algunos sistemas para Pymes locales. Fui pasando de un perfil SysAdmin a uno de Desarrollo sin dejar las escuelas y los proyectos de gestión tecnológica. Traduzco del inglés cosas que me interesan, como a McKenzie Wark, la documentación de Python o [alguno de estos artículos](https://alvarmaciel.gitlab.io/cyberiada/categories/traducciones/).

Sostengo dos sitios en internet. [Acerca De La Educación](https://acercadelaeducacion.com.ar) que está vivo desde el 2006. Fue perdiendo impulso, pero conservo su historia. Y [Cyberiadas](https://alvarmaciel.gitlab.io/cyberiada/) un sito de nerdeadas y pasiones.


Colaboré en muchas charlas de Software Libre y de Educación. Estas son algunas:

- 2020 Reagrupaciones escolares durante promoción acompañada. Ciclo de charlas Normal 7
- 2019 [¿Cuán inteligente y cuán artificial son las «inteligencias artificiales?](http://www.ciacentro.org.ar/node/2498). Centro de investigaciones Artísticas
- 2018 [Pensamiento computacional a través de plataformas de computación física](https://udesa.edu.ar/semana-de-la-educacion). Universidad de San Andrés
- 2018 “Introducción a Desmos. Matemática con computadoras” Exposición cierre Programa de Aceleración. C. A. B. A
- 2018 “Enredados en las redes. Cómo abordar en las familias el uso de tecnología”. Escuela Normal Superior N.º 1 Pte. Roque Saenz Peña
- 2018 “Internet y nosotres.  Una relación compleja” Taller de ingreso al nivel medio  Escuela Normal Superior N.º 1 Pte. Roque Saenz Peña
- 2018 “Redes es Infancia” El futuro llegó hace rato. Escuela Cooperativa Mundo Nuevo
- 2017 “Redes es Infancia” El futuro llegó hace rato. Escuela Cooperativa Mundo Nuevo
- 2016 SqueakfestTaller “Github para maestrxs. Markdown para todxs”
- 2014 Mozfest  Representations of Web Literacy Map
- 2013 Scrum Lider MozFest Empowering Diverse Audiences | LOCALIZING WEBMAKER 
- 2012 Expositor EduJam  Modelo de trabajo en escuela primaria
- 2012 Disertante FLISOL 2012: Acerca de la innovación en la Escuela
- 2010 Disertante. Simposio de Software Libre y Educación: Tuquito en Classmate  Facultad de Filosofía y Letras (UBA).
- 2010 Expositor de funcionamiento de sistema operativo Linux en dispositivos classmate. Universidad de la Matanza.
